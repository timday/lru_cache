/******************************************************************************/
/*  Copyright (c) 2010-2011, Tim Day <timday@timday.com>                      */
/*                                                                            */
/*  Permission to use, copy, modify, and/or distribute this software for any  */
/*  purpose with or without fee is hereby granted, provided that the above    */
/*  copyright notice and this permission notice appear in all copies.         */
/*                                                                            */
/*  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  */
/*  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF          */
/*  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR   */
/*  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN     */
/*  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF   */
/*  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.            */
/******************************************************************************/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE lru_perf

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>
#include <boost/random.hpp>
#include <iostream>
#include <set>
#include <vector>

#include "lru_cache.h"
#include "scoped_timer.h"

// Random number source for generating test data
boost::mt19937 rng(/*seed=*/23);
boost::random_number_generator<
  boost::mt19937,size_t
  > rnd(rng);

// Functions to create collections of test keys

// General pattern
template <typename K> std::vector<K> make_keys(size_t);

// Integer specialization: 
// generate n unique integer keys
template <> std::vector<int> make_keys<int>(size_t n)
{
  std::set<int> r;
  do {
    r.insert(static_cast<int>(rng()));
  } while (r.size()<n);
  return std::vector<int>(r.begin(),r.end());
}

// String specialization: 
// generate n unique long strings
template <> std::vector<std::wstring> make_keys<std::wstring>(size_t n)
{
  // Sufficiently long key lengths favour comparator 
  // based containers over hashed
  const size_t keylength=256;

  std::set<std::wstring> r;
  do {
    std::wstring s;
    for (size_t i=0;i<keylength;++i)
      s+=static_cast<wchar_t>(1+rnd(65535));
    r.insert(s);
  } while(r.size()<n);
  return std::vector<std::wstring>(r.begin(),r.end());
}

// Extract relevant part of __PRETTY_FUNCTION__ text
std::string shrink_pretty_function_text(
  const std::string& s
) {
  std::string r;
  int c=0;
  for (size_t i=s.find("=")+2;i<s.size()-1;++i) {
    if (c<=1) r+=s[i];
    if (s[i]=='<') ++c;
    if (s[i]=='>') {if (c==2) r+="...>";--c;}
  }
  return r;
}

// Test the cache object (capacity n) with k keys
// and a accesses.  Benchmark adapts to CACHE::key_type, 
// but expects an int-like CACHE::value_type
template <typename CACHE> void benchmark(
  CACHE& lru,
  size_t n,
  size_t k,
  size_t a
) {

  typedef typename CACHE::key_type key_type;

  // The set of m keys to be used for testing
  std::vector<key_type> keys=make_keys<key_type>(k);

  // Create a random access sequence plus some "primer"
  std::vector<key_type> seq;
  for (size_t i=0;i<a+n;++i) 
    seq.push_back(keys[i%k]);
  std::random_shuffle(seq.begin(),seq.end(),rnd);

  // Prime the cache so timed results reflect
  // "steady state", not ramp-up period.
  for (size_t i=0;i<n;++i)
    lru(seq[i]);
  
  // Setup timing
  scoped_timer timer(
    boost::str(
      boost::format("%1% load %|2$4.1f|%%")
      % shrink_pretty_function_text(__PRETTY_FUNCTION__)
      % (k*100.0/n)
    ),
    "Maccesses",
    a/static_cast<float>(1<<20)
  );
  
  // Run the access sequence
  int t=0;
  for (
    typename std::vector<key_type>::const_iterator it=
      seq.begin()+n;
    it!=seq.end();
    ++it
  )
    t+=lru(*it);

  // Avoid "optimised away"
  volatile int force=0;
  force=t;
}

// Classes we want to test
typedef boost::mpl::list<
  lru_cache_using_std_map<int,int>::type,
  lru_cache_using_std_unordered_map<int,int>::type,
  lru_cache_using_boost_set<int,int>::type,
  lru_cache_using_boost_unordered_set<int,int>::type,

  lru_cache_using_std_map<std::wstring,int>::type,
  lru_cache_using_std_unordered_map<std::wstring,int>::type,
  lru_cache_using_boost_set<std::wstring,int>::type,
  lru_cache_using_boost_unordered_set<std::wstring,int>::type
> test_types;

// Some lightweight functions to cache
// for the key types of interest

template <typename K> int fn(const K&);

template <> int fn<int>(const int& x) {
  return x;
}

template <> int fn<std::wstring>(const std::wstring& x) {
  return x.size();
}

BOOST_AUTO_TEST_CASE_TEMPLATE
(
  lru_perf,
  CACHE,
  test_types
) {
  const size_t n=1024;

  CACHE lru(fn<typename CACHE::key_type>,n);

  // Test a variety of cache load levels
  benchmark(lru,n,n/2    ,1024*n);
  benchmark(lru,n,(7*n)/8,1024*n);
  benchmark(lru,n,n      ,1024*n);
  benchmark(lru,n,(9*n)/8,1024*n);
  benchmark(lru,n,2*n    ,1024*n);
  std::cout << std::endl;
}
