LRU cache implementation in C++
===============================

For the convenience of users of this repository, this directory
contains the HTML and PDF versions of the accompanying article
text (processed from LaTeX sources, which are not supplied); also
hosted at http://timday.bitbucket.org/lru.html

The source license does not apply to the non-source article content
which is Copyright 2010-2011 T Day.
